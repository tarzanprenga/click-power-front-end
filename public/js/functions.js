function toJSON(str){
    var json;
    try{
        json = JSON.parse(str);
    }catch(e){
        return false;
    }
    return json;
}