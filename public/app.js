new Vue({
    router: router,
    data: {
        eventBus: new Vue(),
        width_fix: 0,
        title: "",
        showMenu: false
    },
    methods: {
    },
    created: function() {
        var vm = this;
        this.eventBus.$on('navbar.toggled', function() {
            vm.showMenu = !vm.showMenu;
        });

        var vm = this;
        this.eventBus.$on('topbar.changeTitle', function(title) {
            vm.title = title;
        });
    },
    mounted: function() {
        this.eventBus.$emit('app.mounted');
    }
}).$mount("#app");