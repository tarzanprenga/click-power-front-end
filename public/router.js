var router = new VueRouter({
    mode: 'history',
    base: '/panel/',
    routes: [
        { name: 'home',         path: '/',                    component: homeComponent },
        { name: 'dashboard',    path: '/dashboard',           component: dashboardComponent },
        { name: 'statistics',   path: '/statistics',          component: statisticsComponent },
        { name: 'ads',          path: '/ads',                 component: adsComponent },
        { name: 'numbers',      path: '/numbers',             component: numbersComponent },
        { name: 'settings',     path: '/settings',            component: settingsComponent },
        { name: 'install',      path: '/settings/install',    component: settingsInstallComponent },
        { name: 'config',       path: '/settings/config',     component: settingsConfigComponent },
        { name: 'password',     path: '/settings/password',   component: settingsPasswordComponent },
        { name: 'notfound',     path: '/*',                   redirect: {name: 'home'} },
    ]
});