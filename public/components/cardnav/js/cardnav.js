var cardnavItemComponent = Vue.component('cardnav-item', {
    template: `
        <div class="box">
            <div class="content" @click="navigate(route)">
                <span class="glyphicon glyp-cardnav-item" v-bind:class="'glyphicon-'+glyphicon"></span>
                <div class="cardnav-item-title">{{title}}</div>
            </div>
        </div>
    `,
    props: ['title', 'glyphicon', 'route'],
    methods: {
        navigate: function(route) {
            this.$router.push({ name: route });
        }
    }
});

var cardnavComponent = Vue.component('cardnav', {
    template: `
        <div class="cardnav-container">
            <slot></slot>
        </div>
    `,
    props: ['eventBus'],
    data: function() {
        return {
        };
    },
    methods: {
    },
    created: function() {
    },
    destroyed: function() {
    },
    mounted: function() {
    }
});