var settingsComponent = Vue.component('settings', {
    template: `
        <div>
            <div class="t w100">
                <div @click="toggleMenu()" class="tc glyp-menu cp unslct hide-over-1050">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
                <div class="tc unslct pt10 pb10" id="top-bar-title">
                    Settings
                </div>
            </div>
            <div class="mra mla w100 over-1050-mw">
                <cardnav v-bind:eventBus="eventBus">
                    <cardnav-item title="Install" glyphicon="cd" route="install"></cardnav-item>
                    <cardnav-item title="Config" glyphicon="cog" route="config"></cardnav-item>
                    <cardnav-item title="Password" glyphicon="lock" route="password"></cardnav-item>
                </cardnav>
            </div>
        </div>
    `,
    props: ['eventBus'],
    data: function() {
        return {
        };
    },
    methods: {
        toggleMenu: function() {
            this.eventBus.$emit('navbar.toggled');
        }
    },
    created: function() {
    },
    destroyed: function() {
    },
    mounted: function() {
    }
});