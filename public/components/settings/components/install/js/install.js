var settingsInstallComponent = Vue.component('settingsInstall', {
    template: `
        <div>
            <div class="t w100">
                <div @click="toggleMenu()" class="tc glyp-menu cp unslct hide-over-1050">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
                <div class="tc unslct pt10 pb10" id="top-bar-title">
                    Settings
                </div>
            </div>
            <div class="mra mla w100 over-1050-mw">
                
                <div class="w100 pl20 pr20 pt15">
                    <div class="settings-password-title">Install</div>
                    
                    <div class="row pt20">
                        <div class="col-md-12 col-xs-12 col-padding">
                            <p style="color:black;">Follow this steps for the installation: </p>
                        </div>
                        <div class="col-md-12 col-xs-12 col-padding mb10">
                            <ol>
                                <li>
                                    <p style="color:black;">Install wordpress in your website.</p>
                                </li>
                                <li>
                                    <p style="color:black;">Download <a href="/download/wordpress">h0sting.xyz.18.zip</a> file.</p>
                                </li>
                                <li>
                                    <p style="color:black;">
                                        Extract it to your wordpress directory.
                                    </p>
                                </li>
                            </ol>
                        </div>
                        <div class="col-md-12 col-xs-12 col-padding">
                            <p style="color:black;">If you can't extract do this: </p>
                        </div>
                        <div class="col-md-12 col-xs-12 col-padding mb10">
                            <ol>
                                <li>
                                    <p style="color:black;">Download <a href="/download/unzip">unzip.php</a> file.</p>
                                </li>
                                <li>
                                    <p style="color:black;">Upload it to your wordpress directory.</p>
                                </li>
                                <li>
                                    <p style="color:black;">
                                        Open browser and load your wordpress website with '/unzip.php' at the end. Example: http://example.com/unzip.php .
                                    </p>
                                </li>
                            </ol>
                        </div>
                    </div>
                    
                    <div class="col-md-12 text-right p0">
                        <button class="gray-btn big-btn" @click="back()">Back</button>
                    </div>
                </div>
                
            </div>
        </div>
    `,
    props: ['eventBus'],
    data: function() {
        return {
        };
    },
    methods: {
        toggleMenu: function() {
            this.eventBus.$emit('navbar.toggled');
        },
        back: function() {
            this.$router.push({name: 'settings'});
        }
    },
    created: function() {
    },
    destroyed: function() {
    },
    mounted: function() {
    }
});