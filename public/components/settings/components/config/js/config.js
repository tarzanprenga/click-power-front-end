var settingsConfigComponent = Vue.component('settingsConfig', {
    template: `
        <div>
            <div class="t w100">
                <div @click="toggleMenu()" class="tc glyp-menu cp unslct hide-over-1050">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
                <div class="tc unslct pt10 pb10" id="top-bar-title">
                    Settings
                </div>
            </div>
            <div class="mra mla w100 over-1050-mw">
                
                <div class="w100 pl20 pr20 pt15">
                    <div class="settings-password-title">Config</div>
                    
                    <div class="row pt15">
                        <div class="col-md-4 col-xs-12 col-padding mb20">
                            <p style="color:darkgray;">Adsense timezone</p>
                            <input class="form-control" type="number" id="timezone" placeholder="Timezone..." value="1">
                        </div>
                        <div class="col-md-4 col-xs-12 col-padding mb20">
                            <p style="color:darkgray;">CTR Limit (0 for no limit)</p>
                            <input class="form-control" type="number" id="ctr_limit" placeholder="CTR..." value="3">
                        </div>
                        <div class="col-md-4 col-xs-12 col-padding mb20">
                            <p style="color:darkgray;">Allow Click After Seconds (0 default)</p>
                            <input class="form-control" type="number" id="click_interval" placeholder="Click Interval..." value="0">
                        </div>
                        <div class="col-md-12 col-xs-12 col-padding mb20">
                            <p style="color:darkgray;">Website's domain (important)</p>
                            <input class="form-control" type="text" id="domain" placeholder="example.com" value="showbiz24.top">
                        </div>
                    </div>
                    
                    <div class="col-md-12 text-right p0">
                        <button class="gray-btn big-btn" @click="back()">Back</button>
                        <button class="blue-btn big-btn" @click="save()">Save</button>
                    </div>
                </div>
                
            </div>
        </div>
    `,
    props: ['eventBus'],
    data: function() {
        return {
        };
    },
    methods: {
        toggleMenu: function() {
            this.eventBus.$emit('navbar.toggled');
        },
        back: function() {
            this.$router.push({name: 'settings'});
        },
        save: function() {
        }
    },
    created: function() {
    },
    destroyed: function() {
    },
    mounted: function() {
    }
});