var settingsPasswordComponent = Vue.component('settingsPassword', {
    template: `
        <div>
            <div class="t w100">
                <div @click="toggleMenu()" class="tc glyp-menu cp unslct hide-over-1050">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
                <div class="tc unslct pt10 pb10" id="top-bar-title">
                    Settings
                </div>
            </div>
            <div class="mra mla w100 over-1050-mw">
            
                <div class="w100 pl20 pr20 pt15">
                    <div class="settings-password-title">Change Password</div>
                    
                    <div class="col-md-12 pt15 p0">
                        Current Password
                    </div>
                    <div class="col-md-12 p0">
                        <input type="text" class="form-control">
                    </div>
                    
                    <div class="col-md-12 pt10 p0">
                        New Password
                    </div>
                    <div class="col-md-12 p0">
                        <input type="text" class="form-control">
                    </div>
                    
                    <div class="col-md-12 pt10 p0">
                        Repeat New Password
                    </div>
                    <div class="col-md-12 p0">
                        <input type="text" class="form-control">
                    </div>
                    
                    <div class="col-md-12 pt15 text-right p0">
                        <button class="gray-btn big-btn" @click="back()">Back</button>
                        <button class="blue-btn big-btn" @click="save()">Save</button>
                    </div>
                </div>
                
            </div>
        </div>
    `,
    props: ['eventBus'],
    data: function() {
        return {
        };
    },
    methods: {
        toggleMenu: function() {
            this.eventBus.$emit('navbar.toggled');
        },
        back: function() {
            this.$router.push({name: 'settings'});
        },
        save: function() {
        }
    },
    created: function() {
    },
    destroyed: function() {
    },
    mounted: function() {
    }
});