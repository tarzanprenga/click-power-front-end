var homeComponent = Vue.component('home', {
    template: `
        <div>
            <div class="t w100">
                <div @click="toggleMenu()" class="tc glyp-menu cp unslct hide-over-1050">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
                <div class="tc unslct pt10 pb10" id="top-bar-title">
                    Home
                </div>
            </div>
            <div class="mra mla w100 over-1050-mw">
                Home
            </div>
        </div>
    `,
    props: ['eventBus'],
    methods: {
        toggleMenu: function() {
            this.eventBus.$emit('navbar.toggled');
        }
    },
    mounted: function() {
    }
});