Vue.component('sidebar', {
    template: `
        <ul class="sidebar-nav">
            <li class="unslct cp" v-bind:class="[{'sidebar-brand': index == 0}, {'sidebar-active': index != 0 && item.route == route}, {'sidebar-open': index != 0 && item.toggled}]" v-for="(item, index) in menu">
                <div @click="navigate(item)">
                    <a>
                        <span v-if="index != 0" class="glyphicon pr5" v-bind:class="'glyphicon-'+item.glyphicon"></span>
                        {{item.name}}
                        <div v-if="index != 0 && hasChildren(item)" class="sidebar-item-dropdown">
                            <span class="glyphicon pr15" v-bind:class="[{'glyphicon-minus': index != 0 && item.toggled}, {'glyphicon-plus': index != 0 && !item.toggled}]"></span>
                        </div>
                    </a>
                </div>
                <div v-if="hasChildren(item)" v-bind:class="{'toggled': item.toggled}" class="sidebar-nav-dropdown">
                    <li v-bind:class="[{'sidebar-active': item.route == route}, {'sidebar-open': item.toggled}]" v-for="item in item.children">
                        <div>
                            <a @click="navigate(item)">
                                <span class="glyphicon pr5 pl20" v-bind:class="'glyphicon-'+item.glyphicon"></span>
                                {{item.name}}
                            </a>
                        </div>
                    </li>
                </div>
            </li>
        </ul>
    `,
    props: ['eventBus'],
    data: function() {
        return {
            title: "Click Power",
            route: this.$route.name,
            menu: [
                {
                    name: 'Click Power',
                    glyphicon: 'paperclip',
                    route: 'home'
                },
                {
                    name: 'Dashboard',
                    glyphicon: 'paperclip',
                    route: 'dashboard'
                },
                {
                    name: 'Statistics',
                    glyphicon: 'signal',
                    route: 'statistics'
                },
                {
                    name: 'Management',
                    glyphicon: 'folder-open',
                    children: [
                        {
                            name: 'Ads',
                            glyphicon: 'list-alt',
                            route: 'ads'
                        },
                        {
                            name: 'Numbers',
                            glyphicon: 'barcode',
                            route: 'numbers'
                        },
                    ],
                    toggled: false
                },
                {
                    name: 'Settings',
                    glyphicon: 'cog',
                    route: 'settings'
                },
                {
                    name: 'Logout',
                    glyphicon: 'log-out',
                    callback: function() {
                        console.log('logout');
                    }
                },
            ],
        }
    },
    methods: {
        hasChildren: function(item) {
            return (item.children && item.children.length)? true: false;
        },
        navigate: function(item) {
            if (this.hasChildren(item)) {
                item.toggled = !item.toggled;

            } else if (item.route) {
                this.route = item.route;
                this.$router.push({ name: item.route });

            } else if (item.callback) {
                item.callback();
            }
        },
        logout: function() {
            console.log('logout');
        },

        calculateMenuToggle: function() {
            for (var i = 0; i < this.menu.length; i++) {
                this.calculateChildrenToggle(this.menu[i]);
            }
        },
        calculateChildrenToggle: function(item) {
            if (!(item.children && item.children.length)) {
                return false;
            }
            for (var i = 0; i < item.children.length; i++) {
                if (item.children[i].route == this.route) {
                    item.toggled = true;
                    return true;
                }
            }
            for (var i = 0; i < item.children.length; i++) {
                if (this.calculateChildrenToggle(item.children[i])) {
                    item.toggled = true;
                    return true;
                }
            }
            return false;
        }
    },
    created: function() {
        this.calculateMenuToggle();
    },
    mounted: function() {
    }
});